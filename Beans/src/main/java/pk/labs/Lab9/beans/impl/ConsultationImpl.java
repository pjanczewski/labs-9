package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

import java.io.Serializable;
import java.util.Date;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;

public class ConsultationImpl extends Object implements Serializable, Consultation, VetoableChangeListener {

    public String student;
    private Term term = new TermImpl();
    public PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private final VetoableChangeSupport vcs = new VetoableChangeSupport(this);

    public ConsultationImpl() {
    }

    public ConsultationImpl(String student, TermImpl term) {
        this.student = student;
        this.term = term;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    public Term getTerm() {
        return this.term;
    }

    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }

    @Override
    public void setTerm(Term term){
        this.term = term;
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        if(minutes > 0) {
            try {
                vcs.fireVetoableChange("duration", this, minutes);
                this.term.setDuration(this.term.getDuration() + minutes);
            }
            catch(PropertyVetoException e) {
                throw e;
            }
        }
    }

    public void addVetoableChangeListener(VetoableChangeListener listener) {
        vcs.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        vcs.removeVetoableChangeListener(listener);
    }

    public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException {
        ConsultationImpl oldConsultation = (ConsultationImpl)event.getOldValue();
        ConsultationImpl newConsultation = (ConsultationImpl)event.getNewValue();

        long oldConsultationStart = oldConsultation.term.getBegin().getTime();
        long oldConsultationEnd = oldConsultation.term.getEnd().getTime();

        long newConsultationStart = newConsultation.term.getBegin().getTime();
        long newConsultationEnd = newConsultation.term.getEnd().getTime();

        if(oldConsultationStart <= newConsultationStart && oldConsultationEnd >= newConsultationStart) {
            throw new PropertyVetoException("consultation", event);
        } else
        if(oldConsultationStart >= newConsultationStart && newConsultationEnd >= oldConsultationStart) {
            throw new PropertyVetoException("consultation", event);
        }
    }
}

