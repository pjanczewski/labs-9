package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.LinkedList;

public class ConsultationListFactoryImpl implements ConsultationListFactory{
    @Override
    public ConsultationList create() {
        return new ConsultationListImpl();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        if(deserialize){
            try{
                FileInputStream fis = new FileInputStream("consultations.xml");
                BufferedInputStream bis = new BufferedInputStream(fis);
                XMLDecoder xmlDecoder = new XMLDecoder(bis);

                LinkedList<Consultation> consultations = (LinkedList<Consultation>) xmlDecoder.readObject();

                return new ConsultationListImpl(consultations);
            }
            catch(FileNotFoundException ex) {

            }
        }

        return this.create();
    }

    @Override
    public void save(ConsultationList consultationList) {
        LinkedList<Consultation> serializationList = new LinkedList<>();

        for(Consultation consultation: consultationList.getConsultation()){
            serializationList.add(consultation);
        }

        try {
            try (XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(
                    new FileOutputStream("consultations.xml")))) {
                encoder.writeObject(serializationList);
            }
        } catch (FileNotFoundException ex) {

        }
    }
}
