package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Term;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class TermImpl extends java.lang.Object implements Serializable, Term {

    public PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    private Date start;
    private int duration;

    public TermImpl() {
        start = new Date();
    }

    public TermImpl(Date start, int duration) {
        this.start = start;
        this.duration = duration;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    @Override
    public int getDuration() {
        return this.duration;
    }

    @Override
    public void setDuration(int duration) {
        if (duration>0) {
            this.duration = duration;
        }
    }

    @Override
    public Date getEnd() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);
        calendar.add(Calendar.MINUTE, duration);
        return calendar.getTime();
    }

    public Date getBegin() {
        return start;
    }

    public void setBegin(Date start) {
        this.start = start;
    }
}